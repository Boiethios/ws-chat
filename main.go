package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// ServerData is server data
type ServerData struct {
	clients   map[*websocket.Conn]bool
	broadcast chan Message
	upgrader  websocket.Upgrader
}

// Message is sent by clients
type Message struct {
	Username string `json:"username"`
	Message  string `json:"message"`
}

var port = "8080"

func main() {
	data := ServerData{
		clients:   make(map[*websocket.Conn]bool),
		broadcast: make(chan Message),
		upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool { return true },
		},
	}

	// Route index
	fileServer := http.FileServer(http.Dir("./public"))
	http.Handle("/", fileServer)

	// Route websocket
	http.HandleFunc("/ws", data.waitForMessagesFromClient)

	// Ready to listen and dispatch messages
	go data.dispatchMessageToClients()

	// Launch http server
	log.Println("Start http server on port", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func (data *ServerData) waitForMessagesFromClient(w http.ResponseWriter, r *http.Request) {
	ws, err := data.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal("Upgrade: ", err)
	}

	defer ws.Close()

	// Register this new client
	data.clients[ws] = true

	// Wait for messages
	for {
		var msg Message
		err := ws.ReadJSON(&msg)
		log.Println("ReadJSON: received", msg)
		if err != nil {
			log.Println("ReadJSON:", err)
			delete(data.clients, ws)
			break
		}
		// Send this new message through the channel
		data.broadcast <- msg
	}
}

func (data *ServerData) dispatchMessageToClients() {
	for {
		// Get the new message
		msg := <-data.broadcast

		// Dispatch it to each client
		for client := range data.clients {
			err := client.WriteJSON(msg)
			if err != nil {
				log.Println("WriteJSON: ", err)
				client.Close()
				delete(data.clients, client)
			}
		}
	}
}
