all: build
	./main

build: public/chat.js main

public/chat.js: Chat.elm
	elm make --yes Chat.elm --output=public/chat.js

main: main.go
	go build main.go