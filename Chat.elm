module Main exposing (..)

import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import WebSocket


(!) : Model -> Cmd Msg -> ( Model, Cmd Msg )
(!) =
    (,)


(=>) =
    (,)


server : String
server =
    "ws://localhost" ++ ":8080/ws"


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Message =
    { username : String
    , message : String
    }


type alias Model =
    { username : String
    , input : String
    , messages : List Message
    }


init : ( Model, Cmd Msg )
init =
    Model "Anonymous" "" []
        ! Cmd.none



-- UPDATE


type Msg
    = Input String
    | Send
    | Received Message
    | ErrorDecode


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        cmd =
            if msg == Send then
                let
                    message =
                        { username = model.username
                        , message = model.input
                        }
                in
                    WebSocket.send server <| encode model
            else
                Cmd.none

        newModel =
            case msg of
                Input input ->
                    { model | input = input }

                Send ->
                    { model | input = "" }

                Received message ->
                    { model | messages = message :: model.messages }

                ErrorDecode ->
                    model
    in
        newModel ! cmd


encode : Model -> String
encode model =
    let
        message =
            Encode.object
                [ "username" => Encode.string model.username
                , "message" => Encode.string model.input
                ]
    in
        Encode.encode 0 message



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ div [] <| List.reverse <| List.map viewMessage model.messages
        , input [ onInput Input, value model.input, onEnter Send, autofocus True ] []
        , button [ onClick Send ] [ text "Send" ]
        ]


viewMessage : Message -> Html msg
viewMessage message =
    div [] [ text <| "[" ++ message.username ++ "]", text " ", text message.message ]


onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                Decode.succeed msg
            else
                Decode.fail "not ENTER"
    in
        on "keydown" (Decode.andThen isEnter keyCode)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    WebSocket.listen server decode


decode : String -> Msg
decode json =
    let
        decoder =
            Decode.map2 Message
                (Decode.at [ "username" ] Decode.string)
                (Decode.at [ "message" ] Decode.string)
    in
        case Decode.decodeString decoder json of
            Ok message ->
                Received message

            Err _ ->
                ErrorDecode
